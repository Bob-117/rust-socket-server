// std
use std::env;

// custom
mod socket_server;
use socket_server::SocketServer;


// methods
fn process(password: String, host: String, port: String) {
    let s = SocketServer::new(password, host, port);
    s.run();
}

fn get_env_var() {
    //
}

// main

fn main() {

    get_env_var();
    let _pass = env::var("SERVER_PASSWORD").unwrap_or_else(|_| {
        println!("No password provided. Server will run without password protection.");
        "1234".to_string()
    });

    let _host = env::var("SERVER_HOST").unwrap_or_else(|_| {
        println!("default host");
        "127.0.0.1".to_string()
    });

    let _port = env::var("SERVER_PORT").unwrap_or_else(|_| {
        println!("default port");
        "11117".to_string()
    });

    process(_pass, _host, _port);
}