use std::io::Read;
use std::net::{TcpListener, TcpStream};


pub struct SocketServer {
    password: String,
    host: String,
    port: String,
}

trait HandleIncStream {
    fn huho(&self) {}
}

impl HandleIncStream for SocketServer {
    fn huho(&self) {
        println!("huho {}:{}, {}", self.host, self.port, self.password)
    }
}

impl SocketServer {

    pub fn new(password: String, host: String, port: String) -> SocketServer {
        // constructor
        //
        SocketServer { password, host, port }
    }

    fn create(&self) -> TcpListener {
        // bind a TCP listener on self.(host:port)
        let address = format!("{}:{}", self.host, self.port);
        let listener = TcpListener::bind(address ).expect("Failed to bind address");
        println!("running on {}:{}", self.host, self.port);
        return listener;
    }

    fn handle_stream(mut stream: TcpStream) {
        let mut buffer = [0; 1024];
            while let Ok(bytes_read) = stream.read(&mut buffer) {
                if bytes_read == 0 {
                    break;
                }
                let message = String::from_utf8_lossy(&buffer[..bytes_read]);
                println!("Incoming message: {}", message);
        
            }   
    }

    pub fn run(&self) {
        // while !atomic
        
        //TODO try catch, (rebind ? exit ?)
        let _listener = self.create();
        
        for stream in _listener.incoming() {
            match stream {
                Ok(stream) => {
                    self.huho();
                    // move : see lifetime
                    // transfer stream ownsership to the next scope
                    std::thread::spawn(move || {
                        SocketServer::handle_stream(stream);
                    });
                }
                Err(e) => {
                    eprintln!("Error: {}", e);
                }
            }
        }
        
    }
}


